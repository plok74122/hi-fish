require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module NeteaseCorp
  class Application < Rails::Application
    config.generators.test_framework false
    config.autoload_paths += %W(#{config.root}/app/uploaders)
    config.eager_load_paths += %W( #{config.root}/app/jobs)
    config.time_zone = 'Taipei'
    config.exceptions_app = self.routes
    config.i18n.default_locale = 'zh-TW'
  end
end

Date::DATE_FORMATS[:short] = '%Y/%m/%d'
Time::DATE_FORMATS[:short] = '%Y/%m/%d %H:%M'

