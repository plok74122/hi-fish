Rails.application.routes.draw do
  devise_for :users, only: :omniauth_callback, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :users do
    delete 'users/sign-out', to: 'devise/sessions#destroy', as: :destroy_ruby_session
  end
  authenticate :user, lambda {|u| u.legal?} do
    root to: 'targets#index'
    namespace :admin do
      resources :targets, only: [:index, :show, :edit, :update]
    end
    resources :users
    resources :targets do
      member do
        post :force_deliver
      end
    end
    resources :delivery_schedules, only: [:index, :update]
    resources :themes
    resources :smtp_settings, only: [:edit, :update]
    resources :delivery_logs, only: [:index]
    resources :password_lists, only: [:index]
    resources :broadcasts, only: [:index, :create]
    get '/custom_chart' => 'dashboard#custom_chart'
  end
  require 'sidekiq/web'
  authenticate :user, lambda {|u| u.admin?} do
    mount Sidekiq::Web => '/sidekiq'
  end

  scope(path: '/api/v1/', module: 'api_v1', as: 'v1', defaults: { format: :json }) do
    resources :records, only: :create
    resources :send_themes, only: :create
    resources :details, only: :create
  end
end
