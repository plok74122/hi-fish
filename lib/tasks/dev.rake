namespace :dev do
  task fake_targets: :environment do
    Target.all.each {|t| t.destroy}
    User.all.each do |u|
      (10..20).to_a.sample.times do
        u.targets.create(email: Faker::Internet.email)
      end
    end
    Target.all.each do |t|
      (0..2).to_a.sample.times do
        user_agent = UserAgent.parse(Faker::Internet.user_agent)
        t.records.create(
          email: t.email,
          password: Devise::friendly_token(10),
          ip_address: IPAddr.new(Faker::Internet.ip_v4_address).to_i,
          browser: user_agent.browser,
          platform: user_agent.platform,
          user_agent: user_agent
        )
      end
    end
    Target.all.each do |t|
      (1..3).to_a.sample.times do
        user_agent = UserAgent.parse(Faker::Internet.user_agent)
        t.records.create(
          email: t.email,
          ip_address: IPAddr.new(Faker::Internet.ip_v4_address).to_i,
          browser: user_agent.browser,
          platform: user_agent.platform,
          user_agent: user_agent
        )
      end
    end
  end
end