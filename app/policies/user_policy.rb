class UserPolicy < ApplicationPolicy

  def index?
    user.legal?
  end

  def show?
    user.legal?
  end

  def edit?
    user.legal?
  end

  def update?
    user.legal?
  end

  def new?
    user.legal?
  end

  def create?
    user.legal?
  end

  def admin?
    user.admin?
  end

  def manager?
    user.manager?
  end

  def legal?
    user.legal?
  end
end
