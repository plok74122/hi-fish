class DeliverSchedulePolicy < ApplicationPolicy
  def update?
    user == record.user || user.admin?
  end
end
