class Admin::TargetPolicy < ApplicationPolicy
  def index?
    user.manager?
  end

  def show?
    user.manager?
  end

  def edit?
    user.manager?
  end

  def update?
    user.manager?
  end
end
