class TargetPolicy < ApplicationPolicy
  def index?
    user.legal?
  end

  def show?
    record.user == user
  end

  def create?
    user.legal?
  end

  def new?
    user.legal?
  end

  def edit?
    user.manager? || record.user == user
  end

  def update?
    user.manager? || record.user == user
  end

  def force_deliver?
    [1, 2].include?(user.id) && (user.admin? || record.user == user)
  end
end
