module ThemesHelper
  def setup_smtp_setting(theme)
    theme.build_smtp_setting unless theme.smtp_setting
    theme
  end
end
