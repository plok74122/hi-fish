module TargetsHelper
  def targets_filter(options={})
    params.permit(:page, :condition, :email_lists, :user_id).merge(options)
  end

  def netease_email?(email)
    email.include?
  end
end
