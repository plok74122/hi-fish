module SmtpSettingsHelper
  def set_smtp_setting(smtp_setting)
    smtp_setting.accounts.new if smtp_setting.accounts.size == 0
    smtp_setting
  end
end
