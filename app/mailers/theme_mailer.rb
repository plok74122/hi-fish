class ThemeMailer < ApplicationMailer

  def netease(target, theme)
    @email = target.email
    @account_name = target.email.split("@").first
    @encrypt_code = EncryptService.encrypt({ target: { id: target.id } , email: target.email })
    smtp_setting = theme.smtp_setting
    name = theme.name
    subject = theme.subject
    template = theme.template
    @website = theme.website
    subject.gsub! '!@#email#@!', @email
    subject.gsub! '!@#account_name#@!', @account_name

    delivery_options = {
      address: smtp_setting.address,
      port: smtp_setting.port,
      domain: smtp_setting.domain,
      user_name: smtp_setting.user_name,
      password: smtp_setting.password,
      authentication: smtp_setting.authentication,
      enable_starttls_auto: true,
      tls: smtp_setting.tls
    }
    email_options = {
      to: @email,
      subject: subject,
      template_name: "#{template}.html",
      from: "#{name} <#{smtp_setting.accounts.sample.name}@#{smtp_setting.domain}>",
      content_type: 'text/html',
      delivery_method: :smtp,
      delivery_method_options: delivery_options
    }
    begin
      mail(email_options)
    rescue ActionView::MissingTemplate
      attachments ={}
      wording = "Template ID: #{theme.id}(#{subject})！"
      attachments.store(:color, 'danger')
      attachments.store(:text, wording)
      attachments.store(:author_name, 'Template Error!')
      SlackNotifyJob.perform_later(attachments, 'delivery_error')
      raise ActionView::MissingTemplate
    end
  end
end
