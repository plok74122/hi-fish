class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :omniauth_providers => %i[line line_notify]

  serialize :line_raw_info

  has_many :targets
  has_many :broadcasts
  has_many :deliver_schedules, dependent: :destroy

  scope :for_me, -> { where(role: %i[admin employee]) }

  before_create :set_initial_deliver_schedule

  def admin?
    role == 'admin'
  end

  def partner?
    role == 'partner'
  end

  def employee?
    role == 'employee'
  end

  def legal?
    admin? || partner? || employee?
  end

  def manager?
    admin? || employee?
  end

  def self.from_line_omniauth(auth)
    user = User.find_by_line_uid(auth['uid'])
    unless user
      user = User.new
      user.line_uid = auth['uid']
      email = auth['info']['email'] ? auth['info']['email'] : "#{auth['uid']}@friendly.com"
      user.email = email
      user.password = Devise.friendly_token[0, 20]
    end
    credentials = auth['credentials']
    user.line_token = credentials['refresh_token']
    user.line_secret = credentials['secret']
    user.line_credentials = credentials.to_json
    user.line_raw_info = auth['extra']['raw_info'].to_json
    user.save!
    user
  end

  def set_initial_deliver_schedule
    weekdays = %w(0 1 2 3 4 5 6)
    hour = (0..23).to_a
    weekdays.product(hour).each do |weekday, hour|
      deliver_schedules.new(weekday: weekday, hour: hour, available: false)
    end
  end
end
