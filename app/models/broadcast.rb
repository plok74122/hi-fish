class Broadcast < ApplicationRecord
  belongs_to :user
  after_create_commit :send_line_notification

  validates_presence_of :message
  private

  def send_line_notification
    User.all.each do |u|
      LineNotifyJob.perform_later(message, u) if u.line_notify_token
    end
  end
end
