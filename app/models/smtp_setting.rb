class SmtpSetting < ApplicationRecord
  belongs_to :theme
  has_many :accounts , dependent: :destroy

  validates_presence_of :address, :port, :domain, :user_name, :password, :authentication, :tls
  validates_numericality_of :port
  validates_inclusion_of :port, in: (0..65535).to_a
  validates_inclusion_of :tls, in: [true, false]

  accepts_nested_attributes_for :accounts, reject_if: :all_blank, allow_destroy: true
end
