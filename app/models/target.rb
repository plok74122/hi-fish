class Target < ApplicationRecord

  belongs_to :user
  has_many :records, -> {where.not(password: nil).order(created_at: :desc)}, dependent: :nullify
  has_many :details, -> {where(password: nil).order(created_at: :desc)}, class_name: 'Record', dependent: :nullify

  has_many :target_themeships, dependent: :nullify
  has_many :themes, through: :target_themeships

  # validation
  validates :email, uniqueness: { case_sensitive: false, message: '重複的email' }
  validates :email, presence: { message: 'email不能為空' }
  validates_format_of :email, with: /\A([^@\s]+)@(163\.com|126\.com|yeah\.net|188\.com|vip\.163\.com|vip\.126\.com)\z/i, on: :create, message: '這不是一個網易郵箱'
end
