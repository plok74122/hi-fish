class Theme < ApplicationRecord
  has_many :target_themeships, dependent: :nullify
  has_many :targets, through: :target_themeships
  has_one :smtp_setting, dependent: :destroy

  accepts_nested_attributes_for :smtp_setting, reject_if: :all_blank, allow_destroy: true

  # validation
  validates_presence_of :name, :subject, :template, :website
  validates_format_of :website, with: URI::regexp(%w(http https))

  scope :for_all, -> { where(is_public: true) }
  scope :for_me, -> { where(is_public: false) }
end
