class TargetFilter

  attr_accessor :user, :theme, :targets

  def initialize(user, theme)
    @user = user
    @theme = theme
    @targets = user.targets
  end

  def in_control
    @targets = @targets.where(control: true)
  end

  def in_hand
    @targets = @targets.where(control: false)
  end

  def clear_sent_in_two_week
    sent_in_two_week_ids = TargetThemeship.where('created_at >= ?', Time.now-15.days).pluck(:target_id)
    @targets = @targets - Target.where(id: sent_in_two_week_ids)
  end

  def clear_sent_theme_before
    @targets = @targets - @theme.targets
  end

end