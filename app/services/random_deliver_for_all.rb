module RandomDeliverForAll
  def self.deliver_themes
    hour = Time.now.hour
    weekday = Date.today.wday
    delivered_list = Array.new
    Theme.for_all.each do |theme|
      User.includes(:deliver_schedules).where(deliver_schedules: { weekday: weekday, hour: hour }).each do |user|
        next unless user.deliver_schedules.first.available
        filter = TargetFilter.new(user, theme)
        filter.in_hand
        filter.clear_sent_theme_before
        filter.clear_sent_in_two_week
        targets = filter.targets - delivered_list
        targets = targets.sample(user.max_deliver_number)
        targets.each do |target|
          delivered_list << target
          ThemeMailerJob.set(wait: Random.rand(0..59).minutes).perform_later(target, theme)
        end
      end
    end
  end
end