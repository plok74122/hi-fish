class LineNotifyService
  require 'net/http'
  require 'uri'
  URI = URI.parse("https://notify-api.line.me/api/notify")

  def self.send(token, notify_msg)
    request = Net::HTTP::Post.new(URI)
    request['Authorization'] = "Bearer #{token}"
    request.set_form_data(message: notify_msg)
    response = Net::HTTP.start(URI.hostname, URI.port, use_ssl: URI.scheme == "https") do |https|
      https.request(request)
    end
    Rails.logger.info 'notify msg sent!!!!!!!!!'
    response
  end
end