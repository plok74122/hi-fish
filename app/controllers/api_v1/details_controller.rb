class ApiV1::DetailsController < ApiController
  before_action :check_params

  def create
    begin
      info = EncryptService.decrypt(params[:uuid])
      target = Target.find_by(info[:target])
    rescue
      render json: { message: 'bye' }, status: 400
      attachments = {}
      wording = "IP:#{params[:ip]}\n"
      wording += "UserAgent: #{params[:user_agent]}"
      attachments.store(:color, 'danger')
      attachments.store(:text, wording)
      attachments.store(:author_name, 'Decrypt Error!')
      SlackNotifyJob.perform_later(attachments, 'decrypt_error')
      return
    end
    user_agent = UserAgent.parse(params[:user_agent])
    ip_address = IPAddr.new(params[:ip]).to_i

    record = target.details.find_or_create_by(email: target.email,
                                              ip_address: ip_address,
                                              browser: user_agent.browser,
                                              platform: user_agent.platform,
                                              user_agent: params[:user_agent])
    if record.errors.any?
      render json: { message: 'bye' }, status: 400
    else
      render json: { message: 'welcome' }, status: 200
    end
  end

  private

  def check_params
    unless params.has_key?(:uuid) && params.has_key?(:ip) && params.has_key?(:user_agent)
      render json: { message: 'error' }, status: 400
    end
  end
end
