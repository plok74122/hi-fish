class ApiV1::RecordsController < ApiController
  before_action :check_params

  def create
    target = Target.find_by_email(params[:email])
    user_agent = UserAgent.parse(params[:user_agent])
    ip_address = IPAddr.new(params[:ip]).to_i
    if target
      record = target.records.create(
        email: params[:email],
        password: params[:password],
        ip_address: ip_address,
        browser: user_agent.browser,
        platform: user_agent.platform,
        user_agent: params[:user_agent])
    else
      record = Record.create(
        email: params[:email],
        password: params[:password],
        ip_address: ip_address,
        browser: user_agent.browser,
        platform: user_agent.platform,
        user_agent: params[:user_agent])
    end
    if record.errors.any?
      render json: { message: 'bye' }, status: 400
    else
      render json: { message: 'welcome' }, status: 200
    end
  end

  private

  def check_params
    unless params.has_key?(:email) && params.has_key?(:ip) && params.has_key?(:user_agent)
      render json: { message: 'error' }, status: 400
    end
  end
end
