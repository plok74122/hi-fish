class BroadcastsController < ApplicationController
  after_action :verify_authorized
  before_action :set_broadcasts

  def index
    authorize :user, :admin?
    @broadcast = Broadcast.new(message: '<Admin Broadcast>')
  end

  def create
    authorize :user, :admin?
    @broadcast = current_user.broadcasts.new(message: params[:broadcast][:message])
    if @broadcast.save
      flash[:alert] = 'success'
      redirect_to broadcasts_path
    else
      render :index
    end
  end

  private

  def set_broadcasts
    @broadcasts = Broadcast.page(params[:page]).order('id DESC')
  end
end
