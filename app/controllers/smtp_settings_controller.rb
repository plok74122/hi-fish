class SmtpSettingsController < ApplicationController
  before_action :authorize_admin
  after_action :verify_authorized
  before_action :set_smtp_setting

  def edit

  end

  def update
    if @smtp_setting.update(smtp_params)
      flash[:alert] = 'success'
      redirect_to theme_path(@smtp_setting.theme)
    else
      render :edit
    end
  end

  private

  def set_smtp_setting
    @smtp_setting = SmtpSetting.includes(:accounts).find(params[:id])
  end

  def smtp_params
    params.require(:smtp_setting).permit(:address,
                                        :port,
                                        :domain,
                                        :user_name,
                                        :password,
                                        :authentication,
                                        :tls,
                                        :accounts_attributes => [:name, :id, :_destroy])
  end
end
