class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def line
    @user = User.from_line_omniauth(request.env['omniauth.auth'])

    if @user.persisted?
      check_params
      set_flash_message(:notice, :success, kind: 'Line') if is_navigational_format?
    else
      session['devise.line_data'] = request.env['omniauth.auth']
      redirect_to new_user_registration_url
    end
  end

  def line_notify
    token = request.env['omniauth.auth']['credentials']['token']
    current_user.update!(line_notify_token: token)
    flash[:notice] = '成功連動Line通知！'
    redirect_to targets_path
  end

  def failure
    redirect_to root_path
  end

  private

  def check_params
    sign_in_and_redirect @user, event: :authentication #this will throw if @user is not activated
  end

end