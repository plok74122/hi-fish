class UsersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized
  before_action :set_user, only: [:edit, :update]
  def index
    authorize :user, :admin?
    @users = User.all
  end

  def edit
    authorize @user, :admin?
  end

  def update
    authorize @user, :admin?
    if @user.update(params.require(:user).permit(:name, :max_deliver_number))
      flash[:notice] = 'success'
      redirect_to users_path
    else
      render :edit
    end

  end

  private

  def set_user
    @user = User.find(params[:id])
  end
end
