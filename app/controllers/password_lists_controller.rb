class PasswordListsController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized
  before_action :set_targets, only: [:index, :create]

  def index
    authorize :user
  end

  private

  def set_targets
    @targets = current_user.targets.includes(:records).where.not(records: { password: nil}).order(created_at: :desc)
    case params[:condition]
      when 'NotControlled'
        @targets = @targets.where.not(control: true)
      when 'Incontrol'
        @targets = @targets.where(control: true)
    end

    if params[:keyword]
      @targets = @targets.where('targets.email like ?', "%#{params[:keyword]}%")
    end
    @targets = @targets.page(params[:page])
  end
end
