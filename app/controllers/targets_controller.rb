class TargetsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_params, only: :create
  before_action :set_target, only: [:edit, :update, :show, :force_deliver]
  before_action :set_targets, only: [:index, :create]
  after_action :verify_authorized

  include TargetsHelper

  def index
    authorize :target
  end

  def new
    authorize :target
  end

  def create
    authorize :target
    @targets_record = []
    @emails.each do |e|
      @targets_record << Target.create(user: @user, email: e)
      Rails.logger.info "User: #{current_user.name} add #{e} to #{@user.name}"
    end
    render :index
  end

  def show
    authorize @target
  end

  def edit
    authorize @target
  end

  def update
    authorize @target
    @target.control = params[:target][:control]
    @target.save
    redirect_to targets_path(targets_filter)
  end

  def force_deliver
    authorize @target
    theme = Theme.find(params[:theme_id])
    # if @target.themes.include?(theme)
    #   flash[:alert] = '不符合強制發送規則，已經發送過了！'
    # els
    if @target.target_themeships.where('created_at >= ?', Time.now - 15.days).count != 0
      flash[:alert] = '不符合強制發送規則，十五天內有發送紀錄。'
    else
      random = Random.rand(0..6)
      flash[:alert] = "#{@target.email}將於#{random}分鐘後發送題材[#{theme.name}]，請勿重複點擊發信。"
      ForceThemeMailerJob.set(wait: random.minutes).perform_later(@target, theme)
    end
    redirect_to targets_path
  end

  private

  def set_target
    @target = Target.find(params[:id])
  end

  def set_params
    @user = User.find(params[:user_id])
    @emails = params[:email_lists].split("\r\n")
  end

  def set_targets
    @targets = current_user.targets
    case params[:condition]
      when 'NotControlled'
        @targets = @targets.where.not(control: true)
      when 'Incontrol'
        @targets = @targets.where(control: true)
    end

    if params[:keyword]
      @targets = @targets.where('email like ?', "%#{params[:keyword]}%")
    end
    @targets = @targets.page(params[:page])
  end
end
