class DashboardController < ApplicationController
  before_action :authorize_admin

  def custom_chart
    @delivery_count = Array.new
    User.all.each do |u|
      target_ids = u.targets.pluck(:id)
      @delivery_count << [TargetThemeship.where(target_id: target_ids),u.name]
    end
  end
end
