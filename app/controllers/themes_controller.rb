class ThemesController < ApplicationController
  before_action :authenticate_user!, :authorize_admin
  after_action :verify_authorized
  before_action :set_theme, only: [:show, :edit, :update]

  def index
    @themes = Theme.includes(:smtp_setting => [:accounts]).all
  end

  def new
    @theme = Theme.new
  end

  def create
    @theme = Theme.new(params_theme)
    if @theme.save
      flash[:alert] = 'success'
      redirect_to themes_path
    else
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    if @theme.update(params_theme)
      flash[:alert] = 'success'
      redirect_to themes_path
    else
      render :new
    end
  end

  private

  def set_theme
    @theme = Theme.includes(:smtp_setting => [:accounts]).find(params[:id])
  end

  def params_theme
    params.require(:theme).permit(:name,
                                  :subject,
                                  :content,
                                  :website,
                                  smtp_setting_attributes: [:address,
                                                            :port,
                                                            :domain,
                                                            :user_name,
                                                            :password,
                                                            :authentication,
                                                            :tls])
  end
end
