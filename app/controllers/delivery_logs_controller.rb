class DeliveryLogsController < ApplicationController
  before_action :authorize_admin

  def index
    if current_user.admin?
      @target_themeships = TargetThemeship.includes(:theme, target: [:user]).order('id DESC').page(params[:page])
    else
      target_ids = current_user.targets.pluck(:id)
      @target_themeships = TargetThemeship.includes(:theme, target: [:user]).order('id DESC').where(target_id: target_ids).page(params[:page])
    end
  end
end
