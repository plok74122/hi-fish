class DeliverySchedulesController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized

  def index
    authorize :user
  end

  def update
    @schedule = DeliverSchedule.find(params[:id])
    authorize @schedule
    @schedule.available ? @schedule.available = false : @schedule.available = true
    @schedule.save ? flash[:alert] = 'success' : flash[:alert] = 'fail'
    respond_to do |format|
      format.html {redirect_to delivery_schedules_path}
      format.js
    end
  end
end
