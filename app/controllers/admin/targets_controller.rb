class Admin::TargetsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_target, only: [:edit, :update, :show]
  after_action :verify_authorized

  def index
    authorize [:admin, :target]
    @targets = Target.includes(:user).page(params[:page])
    @targets = @targets.where(user_id: params[:user_id]) if params[:user_id]
    case params[:condition]
      when 'NotControlled'
        @targets = @targets.where.not(control: true)
      when 'Incontrol'
        @targets = @targets.where(control: true)
    end
    @targets = @targets.where('email like ?', "%#{params[:keyword]}%") if params[:keyword]
  end

  def show
    authorize [:admin, @target]
    render 'targets/show'
  end

  def edit
    authorize [:admin, @target]
  end

  def update
    authorize [:admin, @target]
    @target.control = params[:target][:control]
    @target.user_id = params[:target][:user_id]
    @target.save
    redirect_to admin_target_path(@target)
  end

  private

  def set_target
    @target = Target.find(params[:id])
  end
end
