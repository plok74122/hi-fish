class SetIpLocationJob < ApplicationJob
  queue_as :default

  def perform(record_id)
    require 'net/http'
    require 'json'
    record = Record.find(record_id)
    location = Net::HTTP.get(URI("https://ipapi.co/#{record.ip_v4}/json/"))
    parse_location = JSON.parse(location)
    record.city = parse_location['city']
    record.region = parse_location['region']
    record.country_name = parse_location['country_name']
    record.country = parse_location['country']
    record.save!
  end
end
