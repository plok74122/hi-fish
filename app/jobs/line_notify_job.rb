class LineNotifyJob < ApplicationJob
  queue_as :default

  def perform(msg, user = nil)
    if user
      line_notify_token = user.line_notify_token
    else
      line_notify_token = User.first.line_notify_token
      msg = "[未知的目標] #{msg}"
    end
    if Rails.env.production?
      LineNotifyService.send(line_notify_token, msg)
    else
      Rails.logger.info("send msg to #{line_notify_token}")
    end
  end
end