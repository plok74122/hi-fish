class SlackNotifyJob < ApplicationJob
  queue_as :default

  def perform(attachments, channel = 'delivery_error', username = '天才小釣手')
    if Rails.env.production?
      notifier = Slack::Notifier.new '',
                                     channel: channel,
                                     username: username
      notifier.ping '', attachments: [attachments]
    else
      Rails.logger.info "Notify slack: #{attachments} (at #{channel} by #{username})"
    end
  end
end
