class ForceThemeMailerJob < ApplicationJob
  queue_as :default

  def perform(target, theme)
    if Rails.env.production?
      begin
        tries ||= 3
        Rails.logger.info("Send Message start: #{target.email}")
        ThemeMailer.netease(target, theme).deliver_now!
      rescue
        tries -= 1
        sleep 5
        Rails.logger.info("Send Message retry: #{target.email}")
        if tries > 0
          retry
        else
          Rails.logger.info("Send message Retry Fail: #{target.email}")
          return
        end
      else
        theme.targets << target
        attachments = {}
        wording = "Success: #{target.email}(#{theme.subject.gsub '!@#email#@!', target.email})！\n"
        attachments.store(:color, 'success')
        attachments.store(:text, wording)
        attachments.store(:author_name, 'Delivery success!')
        SlackNotifyJob.perform_later(attachments, 'delivery_log')
        LineNotifyJob.perform_later(wording, target.user)
        Rails.logger.info("Send Message success: #{target.email}")
      end
    else
      theme.targets << target
      Rails.logger.info "Message in test delivered: #{target.email}"
    end
  end
end
