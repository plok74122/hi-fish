class MaxDeliverOnceTimeToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :max_deliver_number, :integer, default: 0
  end
end
