class CreateSmtpSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :smtp_settings do |t|
      t.string :address
      t.integer :port
      t.string :domain
      t.string :user_name
      t.string :password
      t.string :authentication
      t.boolean :tls
      t.integer :theme_id
      t.timestamps
    end
    add_index :smtp_settings, :theme_id
  end
end
