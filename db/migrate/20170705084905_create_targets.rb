class CreateTargets < ActiveRecord::Migration[5.0]
  def change
    create_table :targets do |t|
      t.string :email, null: false
      t.boolean :control, default: false
      t.integer :user_id
      t.timestamps
    end
  end
end
