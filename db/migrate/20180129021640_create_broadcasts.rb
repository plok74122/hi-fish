class CreateBroadcasts < ActiveRecord::Migration[5.1]
  def change
    create_table :broadcasts do |t|
      t.string :message
      t.integer :user_id
      t.timestamps
    end
    add_index :broadcasts, :user_id
  end
end
