class AddIpLocationForRecord < ActiveRecord::Migration[5.0]
  def change
    add_column :records, :city, :string
    add_column :records, :region, :string
    add_column :records, :country, :string
    add_column :records, :country_name, :string
  end
end
