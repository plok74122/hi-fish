class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :name
      t.integer :smtp_setting_id
      t.timestamps
    end
    add_index :accounts, :smtp_setting_id
  end
end
