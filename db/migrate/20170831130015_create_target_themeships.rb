class CreateTargetThemeships < ActiveRecord::Migration[5.0]
  def change
    create_table :target_themeships do |t|
      t.integer :target_id
      t.integer :theme_id
      t.timestamps
    end
    add_index :target_themeships, :target_id
    add_index :target_themeships, :theme_id
  end
end
