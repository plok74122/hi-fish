class CreateDeliverSchedules < ActiveRecord::Migration[5.1]
  def change
    create_table :deliver_schedules do |t|
      t.integer :weekday
      t.integer :hour
      t.boolean :available, default: false
      t.integer :user_id
      t.timestamps
    end
    add_index :deliver_schedules, :user_id
    add_index :deliver_schedules, :weekday
    add_index :deliver_schedules, :hour
  end
end
