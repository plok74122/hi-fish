class AddPublicToTheme < ActiveRecord::Migration[5.1]
  def change
    add_column :themes, :is_public, :boolean, default: false
  end
end
