class AlertThemeContentToTmaplate < ActiveRecord::Migration[5.0]
  def up
    rename_column :themes, :content, :template
    change_column :themes, :template, :string
  end

  def down
    rename_column :themes, :template, :content
    change_column :themes, :content, :text
  end
end
