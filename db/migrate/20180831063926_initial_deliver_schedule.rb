class InitialDeliverSchedule < ActiveRecord::Migration[5.1]
  def change
    User.all.each do |user|
      user.set_initial_deliver_schedule
      user.save
    end
  end
end
