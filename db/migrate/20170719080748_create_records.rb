class CreateRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :records do |t|
      t.string :email, null: false
      t.string :password
      t.integer :ip_address, limit: 8
      t.string :browser
      t.string :platform
      t.string :user_agent
      t.integer :target_id
      t.boolean :correct, default: nil
      t.timestamps
    end
  end
end
