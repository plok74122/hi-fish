require 'rails_helper'

describe UserPolicy do
  let (:user) {FactoryGirl.create :user}
  subject {UserPolicy}
  permissions  :new?, :create? do
    it 'denies access if not legal' do
      expect(UserPolicy).not_to permit(user)
    end
    it 'allows access for legal' do
      user.role = 'admin'
      expect(UserPolicy).to permit(user)
      user.role = 'partner'
      expect(UserPolicy).to permit(user)
      user.role = 'employee'
      expect(UserPolicy).to permit(user)
    end
  end
  permissions :index?, :show?, :edit?, :update? do
    it 'denies access if not legal' do
      expect(UserPolicy).not_to permit(user)
    end
    it 'allows access for legal' do
      user.role = 'admin'
      expect(UserPolicy).to permit(user)
      user.role = 'partner'
      expect(UserPolicy).to permit(user)
      user.role = 'employee'
      expect(UserPolicy).to permit(user)
    end
  end
  permissions :admin? do
    it 'allows access only for admin' do
      user.role = 'admin'
      expect(UserPolicy).to permit(user)
      user.role = 'partner'
      expect(UserPolicy).not_to permit(user)
      user.role = 'employee'
      expect(UserPolicy).not_to permit(user)
    end
  end
end