require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user) {create(:user)}
  describe ".roles" do
    it 'admin?' do
      expect(user.admin?).to eq(false)
      user.role = 'admin'
      expect(user.admin?).to eq(true)
    end

    it 'partner?' do
      expect(user.partner?).to eq(false)
      user.role = 'partner'
      expect(user.partner?).to eq(true)
    end

    it 'employee?' do
      expect(user.employee?).to eq(false)
      user.role = 'employee'
      expect(user.employee?).to eq(true)
    end

    it 'legal?' do
      expect(user.legal?).to eq(false)
      user.role = 'admin'
      expect(user.legal?).to eq(true)
      user.role = 'partner'
      expect(user.legal?).to eq(true)
      user.role = 'employee'
      expect(user.legal?).to eq(true)
      user.role = nil
      expect(user.legal?).to eq(false)
    end

    it 'manager?' do
      expect(user.manager?).to eq(false)
      user.role = 'admin'
      expect(user.manager?).to eq(true)
      user.role = 'partner'
      expect(user.manager?).to eq(false)
      user.role = 'employee'
      expect(user.manager?).to eq(true)
      user.role = nil
      expect(user.manager?).to eq(false)
    end
  end
end
