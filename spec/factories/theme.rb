FactoryGirl.define do
  factory :theme do
    name Faker::Lorem.sentence
    subject Faker::Lorem.sentence
    template Faker::Lorem.word
    website Faker::Internet.url
    trait :for_all do
      is_public true
    end
    trait :for_me do
      is_public false
    end
  end
end
