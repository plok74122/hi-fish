FactoryGirl.define do
  factory :record do
    user_agent = UserAgent.parse(Faker::Internet.user_agent)
    email Faker::Internet.email
    password Devise::friendly_token(10)
    ip_address IPAddr.new(Faker::Internet.ip_v4_address).to_i
    browser user_agent.browser
    platform user_agent.platform
    user_agent user_agent
  end
end
