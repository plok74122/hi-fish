FactoryGirl.define do
  factory :user do
    email Faker::Internet.email
    password Devise::friendly_token(10)
    max_deliver_number 3
    trait :admin do
      role 'admin'
    end

    trait :partner do
      role 'partner'
    end

    trait :employee do
      role 'employee'
    end
  end
end
