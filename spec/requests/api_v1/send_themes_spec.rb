require 'rails_helper'

RSpec.describe 'SendThemes', type: :request do
  before do
    @user = create(:user)
    @target = create(:target, user: @user)
    @theme = create(:theme)
  end

  describe 'POST /api/v1/send_themes' do
    it 'should return error , if any params or required params' do
      post '/api/v1/send_themes'
      data = { 'message' => 'error' }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)
    end
    it 'should return error , if no three presence key' do
      data = { 'message' => 'error' }

      post '/api/v1/send_themes', params: { uuid: '123456' }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)

      post '/api/v1/send_themes', params: { ip: Faker::Internet.ip_v4_address }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)

      post '/api/v1/send_themes', params: { user_agent: Faker::Internet.user_agent }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)
    end

    it 'should return bye , if uuid can not decrypt' do
      data = { 'message' => 'bye' }

      post '/api/v1/send_themes', params: { uuid: '123456', ip: Faker::Internet.ip_v4_address, user_agent: Faker::Internet.user_agent }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)
    end

    it 'should process when every params correct' do
      data = { 'message' => 'welcome' }
      uuid = EncryptService.encrypt({ target: { id: @target.id }, theme: { id: @theme.id } })
      ip = Faker::Internet.ip_v4_address
      user_agent = Faker::Internet.user_agent
      post '/api/v1/send_themes', params: { uuid: uuid, ip: ip, user_agent: user_agent }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq(data)
      record_last = Record.last
      expect(record_last.target).to eq(@target)
      expect(record_last.user_agent).to eq(user_agent)
      expect(record_last.ip_v4).to eq(ip)
    end
  end
end