require 'rails_helper'

RSpec.describe 'Record', type: :request do
  before do
    @user = create(:user)
    @target = create(:target, user: @user)
  end

  describe 'POST /api/v1/records' do
    it 'should return error , if any params' do
      post '/api/v1/records'
      data = { 'message' => 'error' }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)
    end

    it 'should return error , if no three presence key' do
      data = { 'message' => 'error' }

      post '/api/v1/records', params: { email: Faker::Internet.email }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)

      post '/api/v1/records', params: { ip: Faker::Internet.ip_v4_address }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)

      post '/api/v1/records', params: { user_agent: Faker::Internet.user_agent }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)
    end

    it 'should create a record belong to target , if params has password' do
      post '/api/v1/records', params: { email: @target.email, ip: Faker::Internet.ip_v4_address, user_agent: Faker::Internet.user_agent, password: Devise::friendly_token(10) }
      data = { 'message' => 'welcome' }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq(data)
      expect(Record.last).to eq(@target.records.last)
    end

    it 'should create a record no target, if params has password' do
      post '/api/v1/records', params: { email: Faker::Internet.email, ip: Faker::Internet.ip_v4_address, user_agent: Faker::Internet.user_agent, password: Devise::friendly_token(10) }
      data = { 'message' => 'welcome' }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq(data)
      expect(Record.last.target).to eq(nil)
    end

    it 'should create a detail belong to target , if params has password' do
      post '/api/v1/records', params: { email: @target.email, ip: Faker::Internet.ip_v4_address, user_agent: Faker::Internet.user_agent }
      data = { 'message' => 'welcome' }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq(data)
      expect(Record.last).to eq(@target.details.last)
    end

    it 'should create a detail no target, if params has password' do
      post '/api/v1/records', params: { email: Faker::Internet.email, ip: Faker::Internet.ip_v4_address, user_agent: Faker::Internet.user_agent }
      data = { 'message' => 'welcome' }
      expect(response).to have_http_status(200)
      expect(JSON.parse(response.body)).to eq(data)
      expect(Record.last).to eq(Record.details.first)
    end

    it 'should return bye if email not a email' do
      post '/api/v1/records', params: { email: '12345', ip: Faker::Internet.ip_v4_address, user_agent: Faker::Internet.user_agent, password: Devise::friendly_token(10) }
      data = { 'message' => 'bye' }
      expect(response).to have_http_status(400)
      expect(JSON.parse(response.body)).to eq(data)
    end

  end

end
