require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe RandomDeliverForMe do
  before do
    @admin_user = create(:user, :admin, max_deliver_number: 3)
    @partner_user = create(:user, :partner, email: Faker::Internet.email, max_deliver_number: 3)
    @employee_user = create(:user, :employee, email: Faker::Internet.email, max_deliver_number: 3)
    5.times do
      create(:target, email: Faker::Internet.user_name + "@163.com", user: @admin_user, control: false)
      create(:target, email: Faker::Internet.user_name + "@163.com", user: @partner_user, control: false)
      create(:target, email: Faker::Internet.user_name + "@163.com", user: @employee_user, control: false)
    end
    @theme = create(:theme, :for_me)
    @theme2 = create(:theme, :for_all)
    @admin_user.deliver_schedules.update_all(available: true)
    @partner_user.deliver_schedules.update_all(available: true)
    @employee_user.deliver_schedules.update_all(available: true)
  end

  it 'job is created' do
    ActiveJob::Base.queue_adapter = :test
    expect do
      ThemeMailer.netease(Target.first, @theme).deliver_later
    end.to have_enqueued_job.on_queue('mailers')
  end

  it 'jobs will be created in correct times' do
    ActiveJob::Base.queue_adapter = :test
    RandomDeliverForMe.deliver_themes
    expect(ThemeMailerJob).to have_been_enqueued.exactly(6)
  end

  it 'jobs will be created in correct times when we adjust more theme' do
    ActiveJob::Base.queue_adapter = :test
    @theme2.update(is_public: false)
    RandomDeliverForMe.deliver_themes
    expect(ThemeMailerJob).to have_been_enqueued.exactly(10)
  end

  it 'jobs will not be created if no target' do
    ActiveJob::Base.queue_adapter = :test
    Target.update_all(control: true)
    RandomDeliverForMe.deliver_themes
    expect(ThemeMailerJob).to have_been_enqueued.exactly(0)
  end

end