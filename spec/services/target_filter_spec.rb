require 'rails_helper'

RSpec.describe TargetFilter do

  before do
    @user = create(:user)
    @target_1 = create(:target, email: Faker::Internet.user_name + "@163.com", user: @user, control: true)
    @target_2 = create(:target, email: Faker::Internet.user_name + "@163.com", user: @user, control: true)
    @target_3 = create(:target, email: Faker::Internet.user_name + "@163.com", user: @user, control: false)
    @target_4 = create(:target, email: Faker::Internet.user_name + "@163.com", user: @user, control: false)
    @theme = create(:theme)
  end


  it 'should find in hand targets' do
    filter = TargetFilter.new(@user, @theme)
    filter.in_hand
    expect(filter.targets.count).to eq(2)
    expect(filter.targets).to eq([@target_3, @target_4])
  end

  it 'should find in control targets' do
    filter = TargetFilter.new(@user, @theme)
    filter.in_control
    expect(filter.targets.count).to eq(2)
    expect(filter.targets).to eq([@target_1, @target_2])
  end

  it 'should clear sent before this theme' do
    # TODO fix database not clear bug
    filter = TargetFilter.new(@user, @theme)
    filter.clear_sent_theme_before
    expect(filter.targets.count).to eq(4)
    expect(filter.targets).to eq([@target_1, @target_2, @target_3, @target_4])

    @theme.targets = [@target_1, @target_2]
    filter.clear_sent_theme_before
    expect(filter.targets.count).to eq(2)
    expect(filter.targets).to eq([@target_3, @target_4])
  end

  it 'should clear sent in two weeks' do
    @theme.targets = [@target_1, @target_2]
    TargetThemeship.where(theme_id: @theme.id, target_id: @target_1).update(:created_at => Time.now - 2.days)
    TargetThemeship.where(theme_id: @theme.id, target_id: @target_2).update(:created_at => Time.now - 20.days)
    filter = TargetFilter.new(@user, @theme)
    filter.clear_sent_in_two_week
    expect(filter.targets.count).to eq(3)
    expect(filter.targets).to eq([@target_2, @target_3, @target_4])
  end
end