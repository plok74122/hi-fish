require 'rails_helper'
include ActiveJob::TestHelper

RSpec.describe RandomDeliverForAll do
  before do
    @user = create(:user, max_deliver_number: 3)
    @user2 = create(:user, email: Faker::Internet.user_name + "@163.com", max_deliver_number: 3)
    7.times {create(:target, email: Faker::Internet.user_name + "@163.com", user: @user, control: false)}
    5.times {create(:target, email: Faker::Internet.user_name + "@163.com", user: @user2, control: false)}
    @theme = create(:theme, :for_all)
    @theme2 = create(:theme, :for_all)
    @user.deliver_schedules.update_all(available: true)
    @user2.deliver_schedules.update_all(available: true)
  end

  it 'job is created' do
    ActiveJob::Base.queue_adapter = :test
    expect do
      ThemeMailer.netease(Target.first, @theme).deliver_later
    end.to have_enqueued_job.on_queue('mailers')
  end

  it 'jobs will be created in correct times' do
    ActiveJob::Base.queue_adapter = :test
    RandomDeliverForAll.deliver_themes
    expect(ThemeMailerJob).to have_been_enqueued.exactly(11)
  end

  it 'jobs will not be created if no target' do
    ActiveJob::Base.queue_adapter = :test
    Target.update_all(control: true)
    RandomDeliverForAll.deliver_themes
    expect(ThemeMailerJob).to have_been_enqueued.exactly(0)
  end

  it 'jobs will be not created if the deliver schedule not available' do
    # 2018-09-02 is Sunday
    allow(Time).to receive(:now).and_return('2018-09-02 15:00:00 +0800'.to_time)
    allow(Date).to receive(:today).and_return('Sun, 02 Sep 2018'.to_date)
    ActiveJob::Base.queue_adapter = :test
    @user.deliver_schedules.where(weekday: 0, hour: 15).update_all(available: false)
    RandomDeliverForAll.deliver_themes
    expect(ThemeMailerJob).to have_been_enqueued.exactly(5)
  end
end